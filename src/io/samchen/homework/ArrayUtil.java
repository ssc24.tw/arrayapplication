package io.samchen.homework;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class ArrayUtil {

	InputModel[] inputModelArray = null;

	private void setArray (int[] array) {
		if (array == null) {
			this.inputModelArray = null;
		} else {
			this.inputModelArray = new InputModel[array.length];
			for (int i=0;i<array.length;i++) {
				this.inputModelArray[i] = InputModel.create(i+1, array[i]);
			}
		}

	}
	
	private boolean checkArrayNull() {
		return (this.inputModelArray == null);
	}
	
	private String getErrorMessageSave () {
		return "請先執行[存入陣列]";
	}
	public String saveArray (int[] array) {
		String rtn = "";
		for (int number : array) {
			if (number < 0 || number > 100) {
				rtn = number + ":超出範圍之外";
				break;
			}
		}
		if ("".equals(rtn)) {
			setArray(array);
		} else {
			setArray(null);
		}
		return rtn;
	}
	
	/*
	 * MaxNum
	 */
	public String  getMaxNum() {
		String rtn = "";
		if (checkArrayNull()) {
			rtn = this.getErrorMessageSave();
		} else {
			InputModel[] tmpArray = this.inputModelArray.clone();
			Arrays.sort(tmpArray,Comparator.comparing(InputModel::valueOf) );
			rtn = "最大值是" + tmpArray[tmpArray.length-1].valueOf();
		}		
		return rtn;
	}
	
	public String getRank() {
		StringBuffer rtn = new StringBuffer("排名:");
		if (checkArrayNull()) {
			rtn = rtn.append(getErrorMessageSave());
		} else {
			InputModel[] tmpArray = this.inputModelArray.clone();
			Arrays.sort(tmpArray,Comparator.comparing(InputModel::valueOf));
			Collections.reverse(Arrays.asList(tmpArray)); 

			for (int j=0;j<this.inputModelArray.length;j++) {
				for (int i=0;i<tmpArray.length;i++) {
					if (inputModelArray[j].equals(tmpArray[i])) {
						rtn.append(i + 1);
						if (j < inputModelArray.length -1) {
							rtn.append(",");
						}
					}
				}
			}			
		}		
		return rtn.toString();
	}
	
	public String getLevel() {
		StringBuffer rtn = new StringBuffer();
		Map<String, AtomicLong> holder = new HashMap<String, AtomicLong>();
		
		for (int j=0;j<this.inputModelArray.length;j++) {
			int score = inputModelArray[j].valueOf();
			String level = "";
			if (score >= 90) {
				level = "A";
			} else if (score >= 80) {
				level = "B";
			} else if (score >= 70) {
				level = "C";
			} else if (score >= 60) {
				level = "D";
			} else {
				level = "E";
			}
			if (!holder.containsKey(level)) {
				holder.put(level, new AtomicLong());
			}
			holder.get(level).incrementAndGet();
		}
		
		List<String> list = new ArrayList<String>();
		list.addAll(holder.keySet());
		list.sort(Comparator.comparing(String::valueOf) );
		for (String level : list) {
			rtn.append(level + ":" +  holder.get(level)+",");
		}
		rtn.deleteCharAt(rtn.length()-1);
		return rtn.toString();
	}
	
	public String getPass() {
		StringBuffer rtn = new StringBuffer();
		int passCnt = 0;
		int notPassCnt = 0;
		for (int j=0;j<this.inputModelArray.length;j++) {
			int score = inputModelArray[j].valueOf();
			String level = "";
			if (score >= 60) {
				passCnt = passCnt+1;
			} else {
				notPassCnt = notPassCnt+1;
			}
		}
		
		rtn.append("及格:" + passCnt);
		rtn.append(",不及格:" + notPassCnt);
		return rtn.toString();
	}





	public static void main(String[] args) {
		int[] myArray =  {65,73,45,89,52,95};
		//System.out.println(Math.max(ary[0],ary[1]));
		ArrayUtil au = new ArrayUtil();
		System.out.println(au.saveArray(myArray));;
		System.out.println(au.getMaxNum());
		System.out.println(au.getRank()); 
		System.out.println(au.getLevel()); 
		System.out.println(au.getPass()); 

	}

}
