package io.samchen.homework;

public class InputModel extends AbstractInputModel{

	public InputModel(int inputId,int inputNumber) {
		this.inputId = inputId;
		this.inputNumber = inputNumber;
	}
	public static InputModel create(int inputId,int inputNumber) {
		return new InputModel(inputId,inputNumber);
	}
	
	public int valueOf() {
		return this.inputNumber;
	}
	public int idOf() {
		return this.inputId;
	}
	
	public boolean equals(Object obj) {
	      if (obj == this) {
	         return true;
	      }
	      if (!(obj instanceof InputModel)) {
	         return false;
	      }
	      InputModel inputModel = (InputModel) obj;
	      return Integer.compare(inputId, inputModel.inputId) == 0;
	   }
//	public void setInputNumber(int inputNumber) {
//		this.inputNumber = inputNumber;
//	}
}
