package io.samchen.homework;

import java.awt.EventQueue;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;


import javax.swing.JPanel;
import javax.swing.JRadioButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ArrayUtilView extends JFrame{

	private ArrayUtil au;
	private JTextField textField1;
	private JTextField textField2;
	private JTextField textField3;
	private JTextField textField4;
	private JTextField textField5;
	private JTextField textField6;
	
	private JRadioButton rdbtnOption1;
	private JRadioButton rdbtnOption2;
	private JRadioButton rdbtnOption3;
	private JRadioButton rdbtnOption4;
	
	
	public ArrayUtilView() {
		au = new ArrayUtil();
		setSize(350,400);
		setType(Type.NORMAL);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("陣列應用");
		getContentPane().setLayout(null);
		
		JPanel panel1 = new JPanel();
		panel1.setBounds(34, 48, 148, 198);
		
		JPanel panel2 = new JPanel();
		panel2.setBounds(192, 97, 84, 120);
		getContentPane().add(panel2);
		
		getContentPane().add(panel1);
		
		JLabel lblScore = new JLabel("分數(0-100)");
		lblScore.setBounds(63, 78, 96, 15);
		panel1.add(lblScore);
		
		textField1 = new JTextField();
		textField1.setBounds(63, 97, 96, 21);
		panel1.add(textField1);
		textField1.setColumns(10);
		
		textField2 = new JTextField();
		textField2.setBounds(63, 127, 96, 21);
		panel1.add(textField2);
		textField2.setColumns(10);
		
		textField3 = new JTextField();
		textField3.setBounds(63, 157, 96, 21);
		panel1.add(textField3);
		textField3.setColumns(10);
		
		textField4 = new JTextField();
		textField4.setBounds(63, 187, 96, 21);
		panel1.add(textField4);
		textField4.setColumns(10);
		
		textField5 = new JTextField();
		textField5.setBounds(63, 217, 96, 21);
		panel1.add(textField5);
		textField5.setColumns(10);
		
		textField6 = new JTextField();
		textField6.setBounds(63, 247, 96, 21);
		panel1.add(textField6);
		textField6.setColumns(10);
		
		rdbtnOption1 = new JRadioButton("最大值　");
		panel2.add(rdbtnOption1);
		rdbtnOption2 = new JRadioButton("排名　　");
		panel2.add(rdbtnOption2);
		rdbtnOption3 = new JRadioButton("等級人數");
		panel2.add(rdbtnOption3);
		rdbtnOption4 = new JRadioButton("及格人數");
		panel2.add(rdbtnOption4);
		
		ButtonGroup G = new ButtonGroup();
		G.add(rdbtnOption1);
		G.add(rdbtnOption2);
		G.add(rdbtnOption3);
		G.add(rdbtnOption4);
		JLabel lblMessge = new JLabel("");
		lblMessge.setBounds(34, 257, 245, 40);
		getContentPane().add(lblMessge);
		
		//測試輸入初始化
		//this.setDefaultValue1();
		//this.setDefaultValue2();
		
		JButton btnSaveArray = new JButton("存入陣列");
		btnSaveArray.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					int inputNum1 = Integer.valueOf(textField1.getText());
					int inputNum2 = Integer.valueOf(textField2.getText());
					int inputNum3 = Integer.valueOf(textField3.getText());
					int inputNum4 = Integer.valueOf(textField4.getText());
					int inputNum5 = Integer.valueOf(textField5.getText());
					int inputNum6 = Integer.valueOf(textField6.getText());
					int[] inputArray = {inputNum1,inputNum2,inputNum3,inputNum4,inputNum5,inputNum6};
					String rtnMsg = au.saveArray(inputArray);
					lblMessge.setText(rtnMsg);
				} catch (Exception e2) {
					e2.printStackTrace();
					lblMessge.setText("請先輸入分數(0-100)");
				}

			}
		});
		btnSaveArray.setBounds(192, 39, 87, 23);
		getContentPane().add(btnSaveArray);
		
		JLabel lblOption = new JLabel("選項");
		lblOption.setBounds(202, 72, 96, 15);
		getContentPane().add(lblOption);

		JButton btnCalu = new JButton("計算");
		btnCalu.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String rtnMsg= "";
				if (rdbtnOption1.isSelected()) {
					rtnMsg = au.getMaxNum();
				} else if (rdbtnOption2.isSelected()) {
					rtnMsg = au.getRank();
				} else if (rdbtnOption3.isSelected()) {
					rtnMsg = au.getLevel();
				} else if (rdbtnOption4.isSelected()) {
					rtnMsg = au.getPass();
				} else {
					rtnMsg = "請先選擇 選項";
				}
				lblMessge.setText(rtnMsg);
			}
		});
		btnCalu.setBounds(192, 227, 87, 23);
		getContentPane().add(btnCalu);

	}
	/*
	 * 測資1
	 */
	private void setDefaultValue1() {
		//65,73,45,89,52,95
		textField1.setText("65");
		textField2.setText("73");
		textField3.setText("145");
		textField4.setText("89");
		textField5.setText("52");
		textField6.setText("95");
	}
	/*
	 * 測資2
	 */
	private void setDefaultValue2() {
		//65,73,45,89,52,95
		textField1.setText("65");
		textField2.setText("73");
		textField3.setText("45");
		textField4.setText("89");
		textField5.setText("52");
		textField6.setText("95");
	}
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ArrayUtilView frame = new ArrayUtilView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
